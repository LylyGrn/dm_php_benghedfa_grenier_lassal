<?php 

echo 'La fonction date permet d\'afficher <em>la date et l\'heure du jour</em> sous différents formats : <br/><br/> ';
echo 'Ici avec <strong>"d m y"</strong> elle permet d\'afficher la date en utilisant deux chiffres à chaque fois et commençant par un zéro initial si nécessaire : '.date("d m y ").' . <br/><br/> ';
echo 'Ici le jour <strong>(j)</strong> et le mois <strong>(n)</strong> sont affichés comme précedemment mais sans zéro initial. L\'année <strong>(Y) </strong>utilise quatre chiffres : ' .date("j n Y"). ' . <br/><br/> ';
echo 'Ici le jour <strong>(D)</strong> et le mois <strong>(M)</strong> sont affichés en trois lettres. La dernière donnée <strong>(L)</strong> indique si l\'année est bissextile avec un 1 ou non avec un 0 :  '.date("D M L").' . <br/><br/>  ';
echo 'Ici le jour <strong>(l)</strong> et le mois <strong>(F)</strong> sont en toutes lettres et l\'on affiche aussi un suffixe ordinal <strong>(S)</strong> :  '.date("l F dS").' . <br/><br/>  ';
echo 'Ici une valeur numérique représente les jours de la semaine de 1 à 7 <strong>(N)</strong>, le numéro de semaine de l\'année <strong>(W)</strong>, et le jour de l\'année <strong>(z)</strong> :  '.date("N W z").' . <br/><br/>  ';
echo 'Ici la fonction date permet de connaître le nombre de jours dans le mois <strong>(t)</strong> :  '.date("t").' . <br/><br/>  ';
echo 'Ici, nous avons l\'heure au format 12h <strong>(h)</strong> avec un zéro initial, l\'ante et post meridiem en minuscule <strong>(a)</strong>, les minutes <strong>(i)</strong> et les secondes <strong>(s)</strong> avec un zéro initial :  '.date("h a i s ").' . <br/><br/>  ';
echo 'Ici, l\'heure est au format 12h <strong>(g)</strong> sans le zéro initial et l\'ante et post meridiem en majuscule <strong>(A)</strong> :  '.date("g A").' . <br/><br/>  ';
echo 'Ici, l\'heure est au format 24h sans le zéro initial <strong>(G)</strong> :  '.date("G").' . <br/><br/>  ';
echo 'Ici, l\'heure est au format 24h avec le zéro initial <strong>(H)</strong> :  '.date("H").' . <br/><br/>  ';
echo 'Ici, l\'on affiche l\'identifiant du fuseau horaire <strong>(e)</strong> :  '.date("e").' . <br/><br/>  ';
echo 'Ici, la fonction indique si l\heure d\'été est affichée avec un 1 ou pas avec un 0 <strong>(I)</strong> :  '.date("I").' . <br/><br/>  ';


?>