<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<meta name="author" content="Lysandra" />
		<meta name="keywords" content="Web, PHP, algorithmique" />
		<meta name="description" content="devoir maison en PHP pour le module de l'épreuve E4" />
		<link rel="stylesheet" href="/DM_PHP/include/css/ex2_style.css" />
		<title>DM_PHP : Exo2</title>
	</head>
	<body>

		<?php 
		$p1 = array("prenom"=>"Victor", 
		"nom"=>"Hugo", 
		"photo"=>'http://upload.wikimedia.org/wikipedia/commons/5/5a/Bonnat_Hugo001z.jpg'); 
		
		$p2 = array("prenom"=>"Jean", 
		"nom"=>"de La Fontaine", 
		"photo"=>'http://upload.wikimedia.org/wikipedia/commons/e/e1/La_Fontaine_par_Rigaud.jpg'); 
		
		$p3 = array("prenom"=>"Pierre", 
		"nom"=>"Corneille", 
		"photo"=>'http://upload.wikimedia.org/wikipedia/commons/2/2a/Pierre_Corneille_2.jpg');
		
		$p4 = array("prenom"=>"Jean", 
		"nom"=>"Racine", 
		"photo"=>'http://upload.wikimedia.org/wikipedia/commons/d/d5/Jean_racine.jpg');
		

		$informations = array($p1, $p2, $p3, $p4);
		
		function genererGalerie($tab){
			echo "<table> 
			<caption>Galerie:</caption>
			<th>Prénom</th><th>Nom</th><th>Photo</th>";
			foreach($tab as $val){
				echo "<tr>";
				foreach($val as $k=>$v){
					if($k=="photo"){
						echo '<td><img src="'.$v.'"alt="photo de '.$val['prenom'].' '. $val['nom'].'" /></td>" ';
						
					}
					else{
					echo "<td>$v</td>";
					}
				}
				echo "</tr>";
			}
			echo "</table>";
		}
		;
		
		genererGalerie($informations);
		?>

	</body>
	
	
</html>

