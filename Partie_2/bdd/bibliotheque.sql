--Exercice 1.1: Création d'une base de données: 

CREATE DATABASE bibliotheque charset="utf8";
USE bibliotheque;

--creation des tables

CREATE TABLE abonne(
	id_abonne INT(3) AUTO_INCREMENT,
	prenom VARCHAR(25),
	CONSTRAINT pk_abonne PRIMARY KEY (id_abonne)
)engine=innodb default character set=utf8;

CREATE TABLE emprunt(
	id_emprunt INT(3) AUTO_INCREMENT,
	id_livre INT(3),
	id_abonne INT(3),
	date_sortie DATE,
	date_rendu DATE,
	CONSTRAINT pk_emprunt PRIMARY KEY (id_emprunt)
)engine=innodb default character set=utf8;

CREATE TABLE livre(
	id_livre INT(3) AUTO_INCREMENT,
	auteur VARCHAR(25),
	titre VARCHAR(50),
	CONSTRAINT pk_livre PRIMARY KEY (id_livre)
)engine=innodb default character set=utf8;

--contraintes

ALTER TABLE emprunt ADD CONSTRAINT fk_emprunt_abonne
	FOREIGN KEY(id_abonne) REFERENCES abonne (id_abonne) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE emprunt ADD CONSTRAINT fk_emprunt_abonne1
	FOREIGN KEY(id_livre) REFERENCES livre (id_livre) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE emprunt 
	MODIFY COLUMN date_rendu DATE DEFAULT NULL; 
	
--insertion contenu
--table abonne:

INSERT INTO abonne VALUES(1, 'Guillaume');
INSERT INTO abonne VALUES(2, 'Benoit');
INSERT INTO abonne VALUES(3, 'Chloe');
INSERT INTO abonne VALUES(4, 'Laura');


--table livre: 

INSERT INTO livre VALUES(100, 'GUY DE MAUPASSANT', 'Une vie'); 
INSERT INTO livre VALUES(101, 'GUY DE MAUPASSANT', 'Bel-Ami'); 
INSERT INTO livre VALUES(102, 'HONORE DE BALZAC', 'Le pere Goriot'); 
INSERT INTO livre VALUES(103, 'ALPHONSE DAUDET', 'Le Petit chose'); 
INSERT INTO livre VALUES(104, 'ALEXANDRE DUMAS', 'La Reine Margot'); 
INSERT INTO livre VALUES(105, 'ALEXANDRE DUMAS', 'Les Trois Mousquetaires'); 

--table emprunt: 

INSERT INTO emprunt VALUES(1, 100, 1, '2011-12-17', '2011-12-18');
INSERT INTO emprunt VALUES(2, 101, 2, '2011-12-18', '2011-12-20');
INSERT INTO emprunt VALUES(3, 100, 3, '2011-12-19', '2011-12-22');
INSERT INTO emprunt VALUES(4, 103, 4, '2011-12-19', '2011-12-22');
INSERT INTO emprunt VALUES(5, 104, 1, '2011-12-19', '2011-12-28');
INSERT INTO emprunt VALUES(6, 105, 2, '2012-03-20', '2012-03-26');
INSERT INTO emprunt VALUES(7, 105, 3, '2013-06-13', NULL);
INSERT INTO emprunt VALUES(8, 100, 2, '2013-06-15', NULL);

